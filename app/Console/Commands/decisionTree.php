<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class decisionTree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:dt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to implement decision tree for flights';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startFile = public_path('start.csv');
        if (!file_exists($startFile) || !is_readable($startFile)) {
            return false;
        }

        $headers = null;
        $data = array();
        $handle = fopen($startFile, 'r');
        if ($handle !== false) {
            while (($row = fgetcsv($handle, 100, ',')) !== false) {
                if (!$headers) {
                    $headers = $row;

                } else {
                    $data[] = array_combine($headers, $row);
                }
            }
            fclose($handle);

        }
        $endFile = public_path('end.csv');
        $fromEU = ['LT', 'LV', 'EE'];
        $delay = 3;
        $cancel = 14;

        $file = fopen($endFile, 'w');
        //Do decisions
        foreach ($data as $record) {
            if (in_array($record['country'], $fromEU)) {
                if ((($record['status'] === 'Cancel')) && ($record['details'] <= $cancel)) {
                    fputcsv($file, array_merge($record, ['decision' => 'Y']), " ");
                } elseif ((($record['status'] === 'Delay')) && ($record['details'] >= $delay)) {
                    fputcsv($file, array_merge($record, ['decision' => 'Y']), " ");
                } else {
                    fputcsv($file, array_merge($record, ['decision' => 'N']), " ");
                }

            } else {
                fputcsv($file, array_merge($record, ['decision' => 'N']), " ");
            }
        }
        fclose($file);
    }
}
